# Video resolution upscaling

## Zadání

Na konzultacích bylo domluveno, že resolution upscaling bude prováděn neuronovou
sítí ideálně s 3D konvolučními vrstvami, aby bylo možné využít informace, která
je ve videu oproti statickým obrázkům -- ve videu máme k dispozici několik po
sobě snímků, většinou jen s mírně se lišící scénou.

## Příprava dat

Data pro 2D SRCNN se stahují pomocí Jupyter Notebooku `download_data.ipynb`, kde
se automaticky naškálují do požadované velikosti. Notebooky počítají se
spuštěním na Google Colab, před stahováním dat je potřeba vytvořit na Google
Drive následující adresářovou strukturu:
```
mvi
+- images-480p
+- images-240p
|   +- all
+- images-240p-val
|   +- all
+- frames-480p
+- frames-480p-val
```

Data pro 3D SRCNN je třeba získat ručně, k jejich generování z mp4 videí byl
použit bashový skript `random_seqs.sh`, kterému se jako argument(y) předloží
umístění videí a skript z nich do adresáře `frames` vyrobí trojice snímků z náhodných
pozic videa. Jednotlivé sekvence mezi sebou mají rozestup minimálně 3 minuty.
Sekvence snímků musí být pojmenovány ve formátu `VIDEO_ID-SEQ_ID-FRAME_ID.jpg`,
kde SEQ_ID je označení sekvence v rámci videa a FRAME_ID je pořadí snímku v sekvenci
jdoucí od jedničky (skript `random_seqs.sh` tento formát generuje sám).
Sekvence je třeba je umístit na Drive do adresářů `frames-480p` (trénovací data)
a `frames-480p-val` (validační data).

Příklad dvou sekvencí, na kterých byla síť trénována, naleznete v adresáři `frames-example`.

## Trénování sítě

Trénování sítě probíhá v notebooku `train.ipynb`, kód je průběžně komentován.
Na konci jsou provedeny základní experimenty a vizualizace výsledků.
