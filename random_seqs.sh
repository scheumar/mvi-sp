#!/bin/bash

for file in "$@"; do
	duration=$(ffprobe -v error -select_streams v:0 -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 "$file" | sed -e 's/\(.*\)\..*/\1/')
	framesets=$((($duration - 780) / 60))
	frname=$(shuf -i 100000-999999 -n 1)
	echo "$file = $frname = $duration sec = $framesets framesets"

	for ss in $(shuf -i 180-$(($duration - 600)) -n $framesets | sort -n); do
		ffmpeg -loglevel panic -hide_banner -ss $ss -i "$file" -vframes 3 frames/$frname-$ss-%d.jpg
	done
done
